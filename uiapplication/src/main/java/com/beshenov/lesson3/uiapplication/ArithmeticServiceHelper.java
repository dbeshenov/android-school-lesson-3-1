package com.beshenov.lesson3.uiapplication;

import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;

import com.beshenov.lesson3.Constants;
import com.beshenov.lesson3.model.Operation;

import java.util.List;

/**
 * @author Dmitry Beshenov
 */
public class ArithmeticServiceHelper {
    private static final String ARITHMETIC_SERVICE_PACKAGE = "com.beshenov.lesson3.service";
    private static final String ARITHMETIC_SERVICE_CLS = "com.beshenov.lesson3.service.ArithmeticService";

    private ArithmeticServiceHelper() {

    }

    public static void sendOperation(Context context, double arg1, double arg2, Operation operation, PendingIntent pendingIntent) {
        Intent intent = new Intent();
        intent.setComponent(new ComponentName(ARITHMETIC_SERVICE_PACKAGE,
                                              ARITHMETIC_SERVICE_CLS));
        intent.putExtra(Constants.BUNDLE_KEY_ARG1, arg1);
        intent.putExtra(Constants.BUNDLE_KEY_ARG2, arg2);
        intent.putExtra(Constants.BUNDLE_KEY_OPERATION, operation.ordinal());
        intent.putExtra(Constants.BUNDLE_KEY_PENDING_INTENT, pendingIntent);

        Intent explicitIntent = createExplicitFromImplicitIntent(context, intent);
        if (explicitIntent != null) {
            context.startService(explicitIntent);
        }
    }

    private static Intent createExplicitFromImplicitIntent(Context context, Intent implicitIntent) {
        PackageManager pm = context.getPackageManager();
        List<ResolveInfo> resolveInfo = pm.queryIntentServices(implicitIntent, 0);

        if (resolveInfo == null || resolveInfo.size() != 1) {
            return null;
        }

        ResolveInfo serviceInfo = resolveInfo.get(0);
        String packageName = serviceInfo.serviceInfo.packageName;
        String className = serviceInfo.serviceInfo.name;
        ComponentName component = new ComponentName(packageName, className);
        Intent explicitIntent = new Intent(implicitIntent);
        explicitIntent.setComponent(component);

        return explicitIntent;
    }
}
