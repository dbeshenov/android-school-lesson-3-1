package com.beshenov.lesson3.uiapplication;

import android.app.PendingIntent;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.beshenov.lesson3.Constants;
import com.beshenov.lesson3.model.Operation;

import java.util.ArrayList;
import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    private List<Button>         mButtonsList;
    private TextView             mResultTextView;
    private EditText             mFirstArgEdit;
    private EditText             mSecondArgEdit;
    private View.OnClickListener mOnClickListener;

    public MainActivityFragment() {
        mOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Operation operation = null;
                switch (v.getId()) {
                    case R.id.addButton:
                        operation = Operation.ADD;
                        break;
                    case R.id.subButton:
                        operation = Operation.SUB;
                        break;
                    case R.id.mulButton:
                        operation = Operation.MUL;
                        break;
                    case R.id.divButton:
                        operation = Operation.DIV;
                        break;
                }

                double arg1, arg2;
                try {
                    arg1 = Double.parseDouble(mFirstArgEdit.getText().toString());
                    arg2 = Double.parseDouble(mSecondArgEdit.getText().toString());
                    ArithmeticServiceHelper.sendOperation(getActivity().getApplicationContext(),
                                                          arg1,
                                                          arg2,
                                                          operation,
                                                          createPendingIntent());
                } catch (NumberFormatException e) {
                    Toast.makeText(getActivity(), R.string.error_wrong_number_format, Toast.LENGTH_SHORT).show();
                }
            }
        };
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        initView(view);
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Constants.RESULT_OK) {
            mResultTextView.setText(String.valueOf(data.getDoubleExtra(Constants.BUNDLE_KEY_RESULT, 0.0)));
        } else if(resultCode == Constants.RESULT_ERROR_DIVIDE_BY_ZERO) {
            mResultTextView.setText(R.string.error_divide_by_zero);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        for (Button button : mButtonsList){
            button.setOnClickListener(mOnClickListener);
        }
    }

    @Override
    public void onStop() {
        super.onResume();
        for (Button button : mButtonsList){
            button.setOnClickListener(null);
        }
    }

    private PendingIntent createPendingIntent() {
        return getActivity().createPendingResult(MainActivity.REQUEST_CODE, new Intent(), 0);
    }

    private void initView(View view) {
        mButtonsList = new ArrayList<>();
        mButtonsList.add((Button) view.findViewById(R.id.addButton));
        mButtonsList.add((Button) view.findViewById(R.id.subButton));
        mButtonsList.add((Button) view.findViewById(R.id.mulButton));
        mButtonsList.add((Button) view.findViewById(R.id.divButton));
        mResultTextView = (TextView) view.findViewById(R.id.result);
        mFirstArgEdit = (EditText) view.findViewById(R.id.firstArgEdit);
        mSecondArgEdit = (EditText) view.findViewById(R.id.secondArgEdit);
    }
}
