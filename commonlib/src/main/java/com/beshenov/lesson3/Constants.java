package com.beshenov.lesson3;

public interface Constants {
    String BUNDLE_KEY_ARG1 = "arg 1";
    String BUNDLE_KEY_ARG2 = "arg 2";
    String BUNDLE_KEY_OPERATION = "Operation";
    String BUNDLE_KEY_PENDING_INTENT = "pending intent";
    String BUNDLE_KEY_RESULT = "result";
    int RESULT_OK = 1;
    int RESULT_ERROR_DIVIDE_BY_ZERO = 2;
}
