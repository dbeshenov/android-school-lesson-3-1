package com.beshenov.lesson3.service;

import com.beshenov.lesson3.model.Operation;

public class ArithmeticProcessor {
    private static final double EPSILON = 0x1.0p-53;

    public static double performOperation(Operation operation, double arg1, double arg2) {
        switch (operation) {
            case ADD:
                return arg1 + arg2;
            case SUB:
                return arg1 - arg2;
            case MUL:
                return arg1 * arg2;
            case DIV:
                if ((Math.abs(arg2) <= EPSILON)) {
                    throw new ArithmeticException("divide by zero");
                }
                return arg1 / arg2;
        }
        throw new UnsupportedOperationException("Unsupported operation");
    }
}
