package com.beshenov.lesson3.service;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.beshenov.lesson3.Constants;
import com.beshenov.lesson3.model.Operation;

public class ArithmeticService extends Service {
    private static final String TAG = ArithmeticService.class.getSimpleName();

    public ArithmeticService() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        new Thread(new Worker(intent)).start();
        return START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private class Worker implements Runnable {
        private double mArg1;
        private double mArg2;
        private Operation mOperation;
        private PendingIntent mPendingIntent;

        public Worker(Intent intent) {
            mArg1 = intent.getDoubleExtra(Constants.BUNDLE_KEY_ARG1, 0.0);
            mArg2 = intent.getDoubleExtra(Constants.BUNDLE_KEY_ARG2, 0.0);
            mOperation = Operation.values()[intent.getIntExtra(Constants.BUNDLE_KEY_OPERATION, 0)];
            mPendingIntent = intent.getParcelableExtra(Constants.BUNDLE_KEY_PENDING_INTENT);
        }

        @Override
        public void run() {
            double result = 0;
            int resultCode = 0;
            try {
                result = ArithmeticProcessor.performOperation(mOperation, mArg1, mArg2);
                resultCode = Constants.RESULT_OK;
            } catch (ArithmeticException exception) {
                resultCode = Constants.RESULT_ERROR_DIVIDE_BY_ZERO;
            }
            try {
                Intent resultIntent = new Intent();
                resultIntent.putExtra(Constants.BUNDLE_KEY_RESULT, result);
                mPendingIntent.send(getApplicationContext(), resultCode, resultIntent);
            } catch (PendingIntent.CanceledException e) {
                Log.e(TAG, "CanceledException", e);
            }
        }
    }
}
